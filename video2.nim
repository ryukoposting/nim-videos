# as a refresher on iterators, let's start with some simple
# examples. we'll first create an iterator that counts up
# forever, starting from a given value.

iterator countUp(start: int): int =
  var x = start
  while true:
    yield x
    x += 1

# and another, very similar to countUp. This one will yield
# multiples of a given integer.
iterator multiplesOf(k: int): int =
  for n in countUp(1):
    yield n * k

# if you recall from the last video, there's also a special
# kind of iterator that we can pass around like plain data,
# and it will preserve its state. This is called a closure
# iterator. So, let's write another countUp, but this time
# we will make it a procedure that returns an iterator.
proc countUp(start: int): iterator: int =
  return iterator: int =
    var x = start
    while true:
      yield x
      x += 1

# with that all out of the way, let's dig into our first
# attempt to make an infinite iterator of prime numbers.

# now, before some of you start picking up pitchforks,
# please understand that this first example is a bad example
# by design.

proc primeRelativeTo(n, prime: int): bool =
  for pm in multiplesOf(prime):
    if pm == n:
      return false
    elif pm > n:
      return true

iterator allPrimes(): int =
  var primes = @[2]
  yield 2
  for n in countUp(3):
    var isPrime = true
    for p in primes:
      if not n.primeRelativeTo(p):
        isPrime = false
        break
    if isPrime:
      primes.add(n)
      yield n


# 70.416s for 10,000
# for p in allPrimes():
#   echo p

# the above solution is pretty horrible- if you
# try running it on your machine, you may
# notice that it slows down quite a bit
# as you start getting to larger and larger
# prime numbers. Through extremely un-scientific
# analysis, I found that finding the first 10,000
# primes with this algorithm on my machine took
# about a minute and 10 seconds.
# This algorithm is made even worse by the fact that
# the code is pretty hideous.
# Perhaps worst of all is that this implementation
# isn't /really/ infinite- as we iterate
# through more and more values, that sequence `primes`
# is going to become extremely large- it will be
# infinite as the number of iterations goes to infinity.

# let's look at another way to do this.
# We can say that, for any prime number, all
# multiples of that prime number are not primes.
# for example, 7 is a prime, therefore
# 14, 21, 28, 35, etc. are not primes.

# With that in mind, we can generate
# an infinite sequence of all prime numbers by
# taking an infinite sequence of all positive
# integers (dropping 1), and filtering out all the multiples
# of prime numbers.

# <show visualization>

# so, we start with our list of integers, then
# sift out all the multiples of two.
# then, take the first number remaining in the
# list. That would be three, so three is prime.
# now, rinse and repeat.


# so, we can implement this algorithm exactly as it has
# been described.

proc filterMultiples(nums: iterator: int, x: int): iterator: int =
  return iterator: int =
    for n in nums():
      if n mod x != 0:
        yield n


proc sift(nums: iterator: int) iterator: int =
  return iterator: int =
    yield first
    let rest = filterMultiples(nums, first).sift()
    for p in rest():
      yield p

iterator allPrimes2: int =
  let primes = sift(countUp(2))
  for p in primes():
    yield p

for p in allPrimes2():
  echo p



# now, there's a big problem here. it stack overflows!
# why? each time we get to that filterMultiples(...).sieve()
# line, we're calling sieve again. In other words, for each
# prime we find, a little bit more stuff gets put on the stack.

# eventually, that's not gonna go so hot.

# this is a limitation with closure iterators, and it is an
# important one to remember- it is fine for them to be
# recursive, but you must use must use them wisely.

# of course, this isn't the end of the line.

# let's make an iterator that will yield all the
# factors of an input integer.
# e.g. factors(15) will yield 1, 3, 5, 15
#      factors(9) will yield 1, 3, 9
#      factors(13) will yield 1, 13

proc factors(n: int): iterator(): int =
  result = iterator(): int =
    for x in 1..n:
      if n mod x == 0:
        yield x

iterator primeFactors(n: int): int =
  yield 1
  yield n

proc isPrime(n: int): bool =
  let facs = factors(n)
  for x in primeFactors(n):
    if x != facs():
      return false
  return true

iterator allPrimes3(): int =
  for n in countUp(2):
    if n.isPrime:
      yield n



# obviously, that primeFactors iterator is pretty
# dumb, we don't really need an iterator for this
# part, but it's a demonstration piece. Bear with me.

# 5.960s for 10,000

# var i = 0
# for p in allPrimes3():
#   echo p
#   if i == 10000:
#     break
#   else:
#     i += 1

# alright, great! we've found an alternative way to
# iterate over all the prime numbers, it solves
# all the problems with the first version, and it
# doesn't stack overflow like the second one either.

# This version
# takes (again, by my very un-scientific testing) about
# 6 seconds to find the first 10,000 primes on my
# machine. That's about 10x better than the first
# version. Not only that, but this version is actually
# infinite in practice, since it does not hold onto a
# sequence of values that will grow to infinite size
# as the number of primes yielded goes to infinity.

# so, we've seen three ways to get an infinite list of
# prime numbers using iterators.

# there are tons of ways to do what we've done here.
# while the third example is much faster than the
# first example, it's still woefully slow in comparison
# to some more refined algorithms.

# one in particular, that many of you are probably
# already familiar with, is called the sieve of
# eratosthenes.

# While very
# easy to understand, it is deceptively difficult to
# implement correctly, even in a lazy functional
# language with powerful list comprehension abilities
# like haskell.

# Garrison Jensen has a great little article on this,
# I'll put the link on screen here so you can go
# check it out.
# (see this link: http://www.garrisonjensen.com/2015/05/13/haskell-programs-are-lies.html)

# So, we will not be getting into that today.
# however, it can be done in Nim, and it is done with
# iterators. I encourage the ambitious among you to
# go check it out.
