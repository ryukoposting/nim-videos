1. Iterators as First-Class Citizens: A Nim Perspective
    - we are used to the idea of a function: it's a block
      of code that takes inputs, and gives one or more
      outputs.
    
    - some of those functions return multiple values in
      such a way that we consider them as a continuous
      sequence. For example, a list of strings, or an
      array of integers, or anything of that sort.
    
    - there is an important dichotomy here. We can have
      functions that return multiple values, but we don't
      always think of them as being a continuous group.
      For example, if we have a function that returns
      a tuple of three strings, we generally don't think
      of that in the same way as we would think of a
      function that returns an array containing 3 strings.
    
    - With this difference in mind, we can begin to find
      new ways to reason about these functions.
      What if we took a function that returns a sequence of
      values, and changed it so that it only returns
      one member of the sequence at a time,
      instead of returning them all at once?
    
    - This is the basic concept behind an iterator. Most
      languages support the notion of an iterator in one
      way or another. For example, Java has the Iterator
      interface, and Python has so-called generators.
      
    - In fact, Nim's approach to iterators in their most
      basic form is quite similar to Python generators.
    
    - <do code>
    
    - So, we have introduced Nim iterators, and we've seen
      how they can be used as values- passing closure
      iterators around the same way as we would a closure,
      function pointer, or delegate.
    
    - However, there is something very noteworthy about
      the last example- we have taken an iterator that
      continues forever, and we got the first three values
      from it, without having to perform any kind of
      evaluation to determine what was inside the iterator.
    
    - let's rephrase that: we just looked at the first
      three members of an infinite sequence of strings,
      without having to evaluate the contents of each
      element in the sequence.
    
    - This is an extremely powerful property of iterators-
      we can use them to represent data structures of
      infinite size. In the next video, we will dig deeper
      into this concept, using iterators to create an
      infinite list of prime numbers.

2. Iterators as Infinite Data Structures: A Nim Perspective
    - often times, we have very large sets of data, but
      only need to search through part of that data set to
      find what we need.
        - generating a data structure containing all of the
          data may use a massive amount of memory.
    
    - of course, there are plenty of ways around this issue.
      the feasible solutions will vary depending on the
      application, but there is one way around this problem
      that can be applied to everything.
    
    - In fact, it can even be applied to data sets that are
      infinite. For example, a list of all the prime numbers.
    
    - In this video, we will be taking a closer look at
      iterators
    
    - If you haven't already, I would recommend watching the
      previous video in this series, as it goes over the
      fundamentals of iterators, but we'll do a quick little
      review here as well.
    
    - <do code>


3. compile-time verification using concepts and ranges
    - Generally speaking, the purpose of a static type
      system is to serve as a tool for verifying program
      correctness at compile time.
    
    - Now, there are exceptions to this- C's types really
      only serve to tell the compiler how many bytes need to
      be allocated to a particular variable, and not much else.
    
    - Regardless, one of the great challenges of statically-
      typed languages is making sure that the type system
      is able to deliver on its promise- the type system
      must be expressive enough to describe many behaviors,
      and strong enough to find even tiny mistakes.
    
    - However, the type system also must be ergonomic enough
      to be useful. A type system can be overly verbose, or
      intricate to the point where practicality is lost.
    
    - Nim's type system is static and strong, and it implements
      a few somewhat unique capabilities that make it
      surprisingly powerful.
    
    - Today, we will be going over two of the ways that Nim
      facilitates compile-time verification. We will start
      with concepts, then we'll move on to ranges. Let's begin.


    - So, what is a concept? If your name is Bjarne Stroustrup,
      you're already pretty familiar with them. If you use
      haskell, you probably call them type classes. If you use
      Rust, you call them traits. And if you use Coq, you
      probably aren't watching this video.
    
    - Regardless of what you call them, a concept in programming
      terms is a construct over a type system that allows for the
      formal constraint of types.

    basic examples
      - Show (stringify-able)
        - this is a concept over a single type, defining the
          behavior of a procedure that takes a type of this
          concept. Notice how nothing else that type needs
          to behave in any particular way, and notice how
          we don't need to explicitly inherit Show in order
          for the concept to be valid over a type.
      - Eq
        - A concept can describe a property of a single type
        - or it can describe a property shared between two types
      - Ord (builtin Ordinal is more restrictive, show)
          - show how Eq is a subset of Ord
      - AtMost
      - Tree

    problem: create a binary search tree that
      can work with any type of data where the
      notion of "greater-than/less-than/equal-to"
      is defined

    - Critically, this is not the same thing as an interface,
      an abstract class, or a generic- a concept is not
      a type of its own, but instead serves as a constraint
      over types, describing some property that they
      all share.

    comparison to a Go or Java interface:
      - interfaces pack implementation of an interface
        with the type. this isn't explicitly required
        by Nim concepts or Haskell type classes.

      - with a Java interface, a function that
        compares two Ords will compare two Ords, even
        if the contained type is different.

      - With concepts,
        we can say that a comparison can only occur
        between two Ords of the same concrete type.
        or, if we want to, we can also do something
        more along the lines of interfaces.

   - While concepts can exist over primitive types, there
     is one thing that concepts cannot solve when it
     comes to integer types- overflow.
   
   - Even in languages with type systems that are widely
     respected for their expressiveness and power, integer
     overflow is a real problem.
   
   - Ultimately, there are things that a conventional
     type system cannot verify, and to make it verify these
     things would make the type system and its language
     a nightmare to use.
   
   - Nim isn't out to change the world on this front, but
     it has a fascinating way to create new primitive numerical
     types. Related to the idea of concepts, but orthogonal
     to them, is ranges.
   

4. tree traversals with iterators
    - use concept Ord from previous video
    - todo: level-order (aka breadth-first)
    
    proc nodesAtLevel(node: BinTree, n: int): iterator: BinTree =
      if node.isNil:
        result = iterator: BinTree =
          discard
      elif n == 0:
        result = iterator: BinTree =
            yield node
      else:
        result = iterator: BinTree =
          let
            left = node.left.nodesAtLevel(n - 1)
            right = node.right.nodesAtLevel(n - 1)
          for n in left():
            yield left
          for n in right():
            yield right

5. creating new syntaxes using templates and macros
    - ladder logic as an example of macros
    - dirty templates are dirty, but can be quite powerful
    template `:=`(a, b): untyped {.dirty.} =
      let a = b
      a
    - compare with C preprocessor
    
    - templates are great- they're simple. However, there's
      a much more powerful tool in the Nim arsenal that
      provides some capabilities orthogonal, but closely
      related to templates.

6. creating a simple cooperative multitasking system
    - use macros to override for and while
    - waitUntil
    - forEach

I. convolution and LTI systems
J. creating new syntaxes with macros
K. concepts- AtMost, Ord, stringify-able
