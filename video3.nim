import strformat

# first, let us define the structure of our binary tree.
type
  BinTree = ref object {.acyclic.}
    data: int
    count: int
    left, right: BinTree

proc put(tree: var BinTree, x: int): BinTree =
  result = tree
  if tree.isNil:
    result = BinTree(data: x, count: 1, left: nil, right: nil)
  elif x == tree.data:
    tree.count += 1
  elif x < tree.data:
    tree.left = tree.left.put(x)
  else:
    tree.right = tree.right.put(x)

# saying `tree = tree.put` over and over is going to
# get tedious very quickly, so we can use a template
# to abstract over it.
template add(t: var BinTree, x: int) =
  t = t.put(x)

# with these out of the way, let's make a general
# procedure for creating a new binary tree. Remember,
# we will represent an empty node as nil.
proc newBinTree(ns: varargs[int]): BinTree =
  result = nil
  for n in ns:
    result = result.put(n)


# while we're at it, let's create procedure that will
# produce a string representation of our tree. Here,
# we will see a prefix function for the first time-
# by wrapping the procedure name in backticks, and
# giving exactly one argument, Nim knows that this
# procedure will be called like a prefix operator.
proc `$`(tree: BinTree): string =
  result = ""
  if not tree.isNil and tree.count > 0:
    result = "("
    result.add $tree.left
    result.add fmt"{tree.data} x{tree.count}"
    result.add $tree.right
    result.add ")"


# let's create iterators that will perform various
# common tree traversals. We'll start with inorder
# traversal- that is, left-root-right order.
proc inOrder(tree: BinTree): iterator: int =
  return iterator(): int =
    if not tree.isNil:
      let
        left = tree.left.inOrder
        right = tree.right.inOrder
      for n in left():
        yield n
      for i in 1..tree.count:
        yield tree.data
      for n in right():
        yield n

iterator inOrder(tree: BinTree): int =
  let iter = tree.inOrder()
  for n in iter():
    yield n

# preorder (root-left-right)
proc preOrder(tree: BinTree): iterator: int =
  return iterator(): int =
    if not tree.isNil:
      let
        left = tree.left.preOrder
        right = tree.right.preOrder
      for i in 1..tree.count:
        yield tree.data
      for n in left():
        yield n
      for n in right():
        yield n

iterator preOrder(tree: BinTree): int =
  let iter = tree.preOrder()
  for n in iter():
    yield n

# postorder (left-right-root)
proc postOrder(tree: BinTree): iterator: int =
  return iterator: int =
    if not tree.isNil:
      let
        left = tree.left.postOrder
        right = tree.right.postOrder
      for n in left():
        yield n
      for n in right():
        yield n
      for i in 1..tree.count:
        yield tree.data

iterator postOrder(tree: BinTree): int =
  let iter = tree.postOrder
  for n in iter():
    yield n


var tree = newBinTree(10, 10, 10, 7, 9, 12, 7, 10, 2)
echo tree

# now, let's test.
for n in tree.inOrder:
  echo n
# it's important to note how the procedure that
# creates a closure iterator is separate from the
# iterator call in the above snippet. To restate
# from previous videos, closure iterators preserve
# their state across calls, where as an inline
# iterator (one that is called directly in a for loop)
# cannot be saved- only iterated over.

