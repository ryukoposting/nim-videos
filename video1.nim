#[ 1: baby's first iterators ]#

iterator myFirstIterator(): int =
  yield 1
  yield 2
  yield 3

for n in myFirstIterator():
  echo n


# iterators can also take arguments.
iterator repeat[T](x: T): T =
  while true:
    yield x

# for x in repeat("hello world!"):
#   echo x


# another example...
iterator countTo(x: int): int =
  for i in 1..x:
    yield i

for n in countTo(3):
  echo n

for n in countTo(8):
  echo n

# the examples so far have shown what Nim
# calls "inline iterators."

# notice how the behavior of one call to
# countTo is not affected by the other-
# the second loop starts from 1, instead
# of starting from 10.

# what if we want to iterate for a bit,
# hold on to where we were, then continue
# from where we left off?

# consider closures, function pointers, or perhaps
# "delegates" for C# users. While these
# are three different things,
# they all allow us
# to pass code around like data.

# this very same thing can be done with
# iterators. In Nim, these are called
# "closure iterators."

# Now, let's write a procedure that
# returns a closure iterator that
# mimics our previous countTo iterator.

proc countTo(x: int): iterator: int =
  return iterator: int =
    for i in 1..x:
      yield i

# because we are now returning an
# iterator as a value, we can store it
# and preserve its state.

let counter = countTo(20)

# note the (). we are calling the
# iterator contained by counter,
# not retrieving the value of counter itself.

for n in counter():
  echo n
  if n == 5:
    break

for n in counter():
  if n mod 2 == 0:
    echo n

for n in counter():
  echo "bad bad bad!"


# now, recall our repeat iterator.
# let's make another procedure that
# returns a closure iterator that
# does the same thing.

proc repeat[T](x: T): iterator: T =
  return iterator: T =
    while true:
      yield x

# a closure iterator is just data,
# so we can pass a closure iterator
# as an argument into another function.

# let's make a procedure that takes
# an iterator as an argument, and
# returns another iterator

proc take[T](iter: iterator(): T, n: int): iterator: T =
  return iterator: T =
    var i = 0
    for x in iter():
      if i == n:
        break
      yield x
      i += 1

let threeHellos = repeat("hello, world!").take(3)

# let threeHellos = take(repeat("hello, world!"), 3)

for h in threeHellos():
  echo h
